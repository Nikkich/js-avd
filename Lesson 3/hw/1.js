/*

  Задание написать простой слайдер.

    Есть массив с картинками из которых должен состоять наш слайдер.
    По умолчанию выводим первый элмемнт нашего слайдера в блок с id='slider'
    ( window.onload = func(){...} // window.addEventListener('load', function(){...}) )
    По клику на PrevSlide/NextSlide показываем предыдущий или следующий сладй соответствено.

    Для этого необходимо написать 4 функции которые будут:

    1. Срабатывать на событие load обьекта window и загружать наш слайд по умолчанию.
    2. RenderImage -> Очищать текущий контент блока #slider. Создавать нашу картинку и через метод аппенд чайлд вставлять её в слайдер.
    3. NextSlide -> По клику на NextSilde передавать currentPosition текущего слайда + 1 в функцию рендера
    4. PrevSlide -> Тоже самое что предыдущий вариант, но на кнопку PrevSlide и передавать currentPosition - 1
      + бонус сделать так что бы при достижении последнего и первого слада вас после кидало на первый и последний слайд соответственно.
      + бонсу анимация появления слайдов через навешивание дополнительных стилей

*/

  var OurSliderImages = ['images/cat1.jpg', 'images/cat2.jpg', 'images/cat3.jpg', 'images/cat4.jpg', 'images/cat5.jpg', 'images/cat6.jpg', 'images/cat7.jpg', 'images/cat8.jpg'];
  var currentPosition = 0;
  const sliderBlock = document.getElementById('slider');
  const prevBtn = document.getElementById('PrevSilde');
  const nextBtn = document.getElementById('NextSilde');
  var image = document.createElement('img');

  window.onload = function() {
    image.src = OurSliderImages[currentPosition];
    imgAppear();
    sliderBlock.appendChild(image);
  }

  function renderImage() {
    sliderBlock.removeChild(image);

    image.src = OurSliderImages[currentPosition];
    imgAppear();

    sliderBlock.appendChild(image);
  }

  nextBtn.addEventListener('click', function() {
    currentPosition++;

    if (currentPosition == OurSliderImages.length) {
      currentPosition = 0;
    }

    renderImage();
  });

  prevBtn.addEventListener('click', function() {
    currentPosition--;

    if (currentPosition < 0) {
      currentPosition = OurSliderImages.length-1;
    }

    renderImage();
  });

  function imgAppear() {
    image.style.opacity = '0';

    setTimeout(function() {
      image.style.opacity = '1';
      image.style.transition = 'opacity 1s';
    }, 100);
  }


